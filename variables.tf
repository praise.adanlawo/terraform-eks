variable "region" {
  default = "us-west-2"
}

variable "cluster-name" {
  description = "cluster-name"
  default = "test-eks-cluster-2"
}

variable "cidr-block" {
  description = "cidr-block"
  default = "10.0.0.0/16"
}

variable "private-subnets" {
  description = "private-subnets"
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "public-subnets" {
  description = "public-subnets"
  default = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
}

variable "group-name" {
  description = "the group name for admin access"
  default     = "devops"
}

variable "programmatic-user" {
  description = "the name to give the user with access key and secret key"
  default     = "praise"
}

variable "console-user" {
  description = "the name to give the user with console access"
  default     = "obinna"
}

variable "pgp-key" {
  description = "the name to give the user with access key and secret key"
  default     = "keybase:cloudgirl"
}

variable "password_length" {
  description = "the length of the password string"
  default     = "10"
}

variable "account-id" {
  description = "account id"
  default     = "229982835417"
}





