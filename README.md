# terraform
Extensive Terraform script
This folder contains Terraform configuration scripts

## Prerequisites
```
aws-cli v1.16.226
kubectl v1.16.0
helm v2.14.3
terraform v0.12.16
Argocd CLI
jq
```

# Configure AWS Credentials
$ aws configure
  ``` 
  AWS Access Key ID [****************7CDE]:
  AWS Secret Access Key [****************2NGa]:
  Default region name [eu-west-2]:
  Default output format [None]: 
  ```

## To Deploy the script
```
$ git clone url
$ cd terraform
$ terraform init
$ terraform plan
$ terraform apply
```
## What this Script does
```
It provisions AWS EKS based o configured prerequisites /n
It provisions AWS VPC /n
It installs Helm /n
It provisons an AWS ALB Ingress Controller with Helm /n
It provisions IAM Group with IAM Users (Programmatic Access and Console Access) /n
It Automates Remote State Locking with AWS S3 and Dynamo DB /n
It provosions ArgoCD on the Eks CLuster using Kubectl /n
It implores Gitops by making refrence to an argocd.yaml file in https://git.deimos.co.za/praise/gitops to automate installation of ArgoCD Applications to the ArgoCD installed on the cluster/n
The ArgoCD Applications in the above refrenced repo are: /n
Certmanager /n
Nginx Ingress /n
External DNS /n
```
## Remote State
```
The script has been automated such that the s3 bucket and dynamodb table are created and pushed to a backend.tf file
After running terraform apply successfully, you would be required to reinitialize the project to push local state to the already created remote state by running terraform init
```
## Argo CD
```
This script implores gitops such that after the Argo Cd is instaled here, it installs its ArgoCD applications from a separate managed repo (https://git.deimos.co.za/praise/gitops) which is refrenced from its raw content
```
## Outputs
```
The script terminates with a successful message and outputs. One of which is the encrypted pgp credentials for access created for the IAM users
They have been formatted properly as:
echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" | base64 --decode | keybase pgp decrypt
Run that on the terminal to give the decoded iam user password
```
To destroy run;
```
$ terraform destroy
```