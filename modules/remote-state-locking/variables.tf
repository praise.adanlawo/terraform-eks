variable "region" {

}

variable "versioning" {
  default     = true
  description = "enables versioning for objects in the S3 bucket"
  type        = bool
}


variable "force_destroy" {
  description = "Whether to allow a forceful destruction of this bucket"
  default     = false
  type        = bool
}

variable "create" {
  type        = bool
  default     = true
}

